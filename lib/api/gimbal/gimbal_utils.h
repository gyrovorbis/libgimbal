/*! \file
 *  \brief Utilities module-wide include
 *  \ingroup utils
 *
 *  \author Falco Girgis
 */

#ifndef GIMBAL_UTILS_H
#define GIMBAL_UTILS_H

#include "utils/gimbal_ref.h"
#include "utils/gimbal_timer.h"
#include "utils/gimbal_uuid.h"
#include "utils/gimbal_version.h"
#include "utils/gimbal_option_group.h"
#include "utils/gimbal_cmd_parser.h"
#include "utils/gimbal_date_time.h"
#include "utils/gimbal_bit_view.h"
#include "utils/gimbal_byte_array.h"
#include "utils/gimbal_scanner.h"
#include "utils/gimbal_uri.h"
#include "utils/gimbal_settings.h"

/*! \defgroup utils Utilities
    \brief Auxiliary utility types
*/

#endif // GIMBAL_UTILS_H
